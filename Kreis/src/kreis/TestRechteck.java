package kreis;
import java.awt.Point;

public class TestRechteck {

    public static void main(String[] args) {
        Rechteck z = new Rechteck(7, 12, new Point(2,4));
        
        System.out.println("Kante A: "+z.getA()+" Kante B: "+z.getB());
        System.out.println("Diagonale: "+z.getDiagonale());
        System.out.println("Fl�che: "+z.getFlaeche());
        System.out.println("Umfang: "+z.getUmfang());
    }
}