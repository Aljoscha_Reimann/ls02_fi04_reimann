import java.util.ArrayList;


/**
 * @version 1.0  2021-03-14
 * @author Aljoscha Reimann<br>
 * Diese Klasse repräsentiert ein <B>Raumschiff</B>
 */
public class Raumschiffe {
	private String schiffsname;
	private int energieversorgung;
	private int schutzschilde;
	private int lebenserhaltungsSysteme;
	private int huelle;
	private int photonentorpedos;
	private int reparaturAndroiden;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	public Raumschiffe() {
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	/**
	   * Konstruktor für eine Instanz der Klasse Raumschiffe
	   * @version 1.0  2021-03-14
	   * @author Aljoscha Reimann<br>Starprogrammierer
	   * @param schiffsname schiffsname
	   * @param energieversorgung energieversorgung
	   * @param schutzschilde schutzschilde
	   * @param lebenserhaltungsSysteme lebenserhaltungsSysteme
	   * @param huelle huelle
	   * @param photonentorpedos photonentorpedos
	   * @param reparaturAndroiden reparaturAndroiden
	   */
	public Raumschiffe(String schiffsname, int energieversorgung, int schutzschilde, int lebenserhaltungsSysteme, int huelle, int photonentorpedos, int reparaturAndroiden) {
		this();
		setSchiffsname(schiffsname);
		setEnergieversorgung(energieversorgung);
		setSchutzschilde(schutzschilde);
		setLebenserhaltungsSysteme(lebenserhaltungsSysteme);
		setHuelle(huelle);
		setPhotonentorpedos(photonentorpedos);
		setReparaturAndroiden(reparaturAndroiden);
	}
	
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	
	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	
	public void setLebenserhaltungsSysteme(int lebenserhaltungsSysteme) {
		this.lebenserhaltungsSysteme = lebenserhaltungsSysteme;
	}
	
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}
	
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	
	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}
	
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public int getEnergieversorgung() {
		return this.energieversorgung;
	}
	
	public int getSchutzschilde() {
		return this.schutzschilde;
	}
	
	public int getLebenserhaltungsSysteme() {
		return this.lebenserhaltungsSysteme;
	}
	
	public int getHuelle() {
		return this.huelle;
	}
	
	public int getPhotonentorpedos() {
		return this.photonentorpedos;
	}
	
	public int getReparaturAndroiden() {
		return this.reparaturAndroiden;
	}
	
	public ArrayList<String> getBroadcastKommunikator(){
		return this.broadcastKommunikator;
	}
	
	public ArrayList<Ladung> getLadungsverzeichnis(){
		return this.ladungsverzeichnis;
	}
	/**
	 	* Methode für neue Ladungen
	 	* @param neueLadung neueLadung
	 	*/
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	/**
	   * Methode um den Zustand Auszugeben
	   */
	public void zustandAusgeben() {
		System.out.println("Schiffsname: "+getSchiffsname());
		System.out.println("Energieversorgung: "+getEnergieversorgung()+" %");
		System.out.println("Schutzschilde: "+getSchutzschilde()+" %");
		System.out.println("LebenserhaltungsSysteme: "+getLebenserhaltungsSysteme()+" %");
		System.out.println("Huelle: "+getHuelle()+" %");
		System.out.println("Photonentorpedos: "+getPhotonentorpedos());
		System.out.println("ReparaturAndroiden: "+getReparaturAndroiden());
	}
	/**
	 * Methode um das Ladungsverzeichnis Auszugeben
	 */
	public void ladungsverzeichnisAusgeben() {
		for(Ladung l : this.ladungsverzeichnis) {
			System.out.println("Ladungs Bezeichnung: " + l.getBezeichnung());
			System.out.println("Ladungs Anzahl: " + l.getAnzahl());
		}
	}
	/**
	 * Methode lässt Photonentorpedos Abschießen
	 * @param raumschiff raumschiff
	 */
	public void photonentorpedoAbschießen(Raumschiffe raumschiff) {
		if(this.getPhotonentorpedos() == 0) {
			nachrichtAnAlle("-=*Click*=-");
		}else {
			this.photonentorpedos--;
		}
	}
	/**
	 * Methode lässt Phaserkanone Abschießen
	 * @param raumschiff raumschiff
	 */
	public void phaserkanoneAbschießen(Raumschiffe raumschiff) {
		if (getEnergieversorgung()<50) {
			nachrichtAnAlle("-=*Click*=-");
		}else {
			setEnergieversorgung(getEnergieversorgung()-50);
			raumschiff.trefferVermerken();
		}
	}
	/**
	 * Methode vermerkt treffer
	 */
	public void trefferVermerken() {
		System.out.println(getSchiffsname() +" wurde getroffen!");
	}
	/**
	 * Methode sendet Nachricht an alle
	 * @param nachricht nachricht
	 */
	public void nachrichtAnAlle(String nachricht) {
		System.out.println(nachricht);
	}
}
