/**
 * @version 1.0  2021-03-14
 * @author Aljoscha Reimann<br>
 * Diese Klasse repr�sentiert eine <B>Ladung</B>
 */
public class Ladung {

	private String bezeichnung;
	private int anzahl;

	public Ladung() {
		this.bezeichnung = "";
		this.anzahl = 0;
	}
/**
 * Methode ist die blaupause f�r eine Ladung
 * @param bezeichnung bezeichnung der Ladung
 * @param anzahl Menge der Ladung
 */
	public Ladung(String bezeichnung, int anzahl) {
		setBezeichnung(bezeichnung);
		this.anzahl = anzahl;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;		
	}
	
	public String getBezeichnung() {
		return this.bezeichnung;		
	}
	
	public int getAnzahl() {
		return this.anzahl;
	}		
}
